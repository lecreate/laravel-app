import axios from 'axios';
import {FETCH_FILE, FETCH_FILE_SUCCESS, ADD_FILE, ADD_FILE_SUCCESS, ADD_FILE_ERROR, DELETE_FILE, DELETE_FILE_SUCCESS, DELETE_FILE_ERROR} from './types';

const ROOT_URL = 'http://localhost:8081';

export function addFile(file) {
    return function(dispatch) {
        dispatch({type: ADD_FILE});

        const formData = new FormData();
        formData.append('file', file);

        axios.post(`${ROOT_URL}/api/v1/files`, formData, {
            "Content-Type": "multipart/form-data"
        }).then(response => {
            dispatch(addFileSuccess(response))
        }).catch(error => {
            dispatch(addFileError(error));
        });
    }
}

export function addFileSuccess(response) {
    return {
        type: ADD_FILE_SUCCESS,
        payload: response
    };
}

export function addFileError(error) {
    return {
        type: ADD_FILE_ERROR,
        payload: error
    };
}

export function fetchFiles() {
    return dispatch => {
        dispatch({type: FETCH_FILE});
        axios.get(`${ROOT_URL}/api/v1/files`, {})
            .then(response => {
                dispatch(fetchFilesSuccess(response));
            }).catch(error => {
                console.log(error);
                // TODO implement error handling for this
            });
    };
}

export function fetchFilesSuccess(files) {
    return {
        type: FETCH_FILE_SUCCESS,
        payload: files
    };
}

export function deleteFile(id) {
    return dispatch => {
        dispatch({type: DELETE_FILE});
        axios.delete(`${ROOT_URL}/api/v1/files/${id}`, {})
            .then(response => {
                dispatch(deleteFileSuccess(response));
            }).catch(error => {
                dispatch(deleteFileError(error));
            });
    };
}

export function deleteFileSuccess(response) {
    return {
        type: DELETE_FILE_SUCCESS,
        payload: response
    };
}

export function deleteFileError(response) {
    return {
        type: DELETE_FILE_ERROR,
        payload: response
    };
}