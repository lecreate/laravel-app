import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../actions/index';

class Files extends Component {

    componentWillMount() {
        this.props.fetchFiles();
    }

    componentDidUpdate(prevProps) {
        if (this.props.deletedFile.file === "deleted") {
            this.props.fetchFiles();
        }
    }

    handleDeleteClick(uuid){
        if (confirm('Are you sure?')) {
            this.props.deleteFile(uuid);
        }
    }

    renderFiles(files) {
        return files.map((file) => {
            return (
                <li className="list-group-item" key={file.uuid}>
                    <strong>{file.name}</strong> ({file.size}B) &nbsp;
                    <button className="pull-right btn btn-outline-dark btn-sm" onClick={(e) => this.handleDeleteClick(file.uuid)}>Delete</button>
                </li>
            );
        });
    }

    render(){
        const {files, loading} = this.props.filesList;

        // TODO implement error display

        if (loading === true) {
            return <div className="loader"></div>;
        }
        return (
            <div>
                <br />
                <div className="clearfix"></div>
                    <ul className="list-group">
                    {this.renderFiles(files)}
                </ul>
            </div>
        );

    }
}

function mapStateToProps(state) {
    return {
        filesList: state.files.filesList,
        deletedFile: state.files.deletedFile
    }
}
export default connect(mapStateToProps, actions)(Files);
