<?php

use App\Models\File;
use Faker\Generator as Faker;

$factory->define(File::class, function (Faker $faker)  {
   return [
       'user_id' => null,
       'uuid' => $faker->uuid,
       'name' => $faker->word,
       'type' => $faker->randomElement(explode(',', config('file-upload.types-accepted')))
   ];
});