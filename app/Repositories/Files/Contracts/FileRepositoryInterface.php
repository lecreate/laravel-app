<?php

namespace App\Repositories\Files\Contracts;

use App\Repositories\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use PDOException;

/**
 * Interface FileRepositoryInterface.
 *
 * @package App\Repositories\Files\Contracts
 */
interface FileRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Delete record by uuid
     *
     * @param string $uuid
     * @throws ModelNotFoundException
     * @throws PDOException
     * @return bool
     */
    public function deleteByUuid(string $uuid): bool;

    /**
     * Get all file records
     *
     * @throws PDOException
     * @return Collection
     */
    public function getAll(): Collection;
}
