<?php

namespace App\Entities;

/**
 * Interface EntityInterface.
 *
 * @package App\Entities
 */
interface EntityInterface
{
    /**
     * De-Serializes the json string into it's entity representation.
     *
     * @param string|array $data
     *
     * @return EntityInterface
     */
    public function deserialize($data): EntityInterface;

    /**
     * Returns a json string with only the set fields.
     *
     * @return string
     */
    public function toJson(): string;

    /**
     * Creates An Array From The Entity.
     *
     * @param null $nested
     *
     * @return array
     */
    public function toArray($nested = null): array;
}
