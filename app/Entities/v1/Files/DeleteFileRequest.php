<?php

namespace App\Entities\v1\Files;

use App\Entities\BaseEntity;
use App\Entities\EntityInterface;
use Illuminate\Validation\Validator;

/**
 * Class DeleteFileRequest.
 *
 * @package App\Entities\v1\Files
 */
class DeleteFileRequest extends BaseEntity implements EntityInterface
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $value
     * @return DeleteFileRequest
     */
    public function setUuid(string $value): DeleteFileRequest
    {
        $this->uuid = $value;

        return $this;
    }

    /**
     * @return Validator
     */
    public function validate(): Validator
    {
        $data = [
            'uuid' => $this->uuid
        ];

        $rules = [
            'uuid' => 'required|string'
        ];

        $validator = $this->getValidator($data, $rules);

        return $validator;
    }
}
