<?php

namespace App\Http\Controllers\v1;

use App\Processors\v1\Files\FileProcessorInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class FileController
 * @package App\Http\Controllers\v1
 */
class FileController extends Controller
{
    /**
     * @var FileProcessorInterface
     */
    private $processor;

    public function __construct(FileProcessorInterface $processor)
    {
        $this->processor = $processor;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function get(Request $request): Response
    {
        return $this->processor
            ->setRequest($request)
            ->dispatch();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function post(Request $request): Response
    {
        return $this->processor
            ->setRequest($request)
            ->dispatch();
    }

    /**
     * @param string $uuid
     * @param Request $request
     * @return Response
     */
    public function delete(string $uuid, Request $request): Response
    {
        return $this->processor
            ->setRequest($request)
            ->dispatch();
    }
}
