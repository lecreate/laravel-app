<?php

namespace App\Processors;

use App\Repositories\BaseRepositoryInterface;
use App\Entities\EntityInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 * Class BaseBaseProcessor.
 *
 * @package App\Processors
 */
abstract class AbstractBaseProcessor implements BaseProcessorInterface
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityInterface
     */
    protected $requestEntity;

    /**
     * @var EntityInterface
     */
    protected $responseEntity;

    /**
     * @var BaseRepositoryInterface
     */
    protected $repository;

    /**
     * Sets the laravel request for the processor instance.
     *
     * @param Request $request
     * @return BaseProcessorInterface
     */
    public function setRequest(Request $request): BaseProcessorInterface
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Sets the request entity for the processor interface.
     *
     * @param string $entity
     * @return BaseProcessorInterface
     */
    public function setRequestEntity(string $entity): BaseProcessorInterface
    {
        $this->requestEntity = new $entity();

        return $this;
    }

    /**
     * Sets the response entity for the processor interface.
     *
     * @param string $entity
     * @return BaseProcessorInterface
     */
    public function setResponseEntity(string $entity): BaseProcessorInterface
    {
        $this->responseEntity = new $entity();

        return $this;
    }

    /**
     * Sets the repository for the processor instance.
     *
     * @param BaseRepositoryInterface $repository
     * @return BaseProcessorInterface
     */
    public function setRepository(BaseRepositoryInterface $repository): BaseProcessorInterface
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * Kicks off the processor and triggers the application to start.
     *
     * @return Response
     */
    abstract public function dispatch(): Response;

    /**
     * Validates the request object with the provided rules.
     *
     * @param array $rules
     * @param array $data
     * @param array $messages
     *
     * @throws ValidationException
     */
    protected function validateRequest(array $data, array $rules, array $messages = [])
    {
        $validator = app(Factory::class);
        $validation = $validator->make($data, $rules, $messages);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
    }
}
