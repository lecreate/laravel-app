<?php

namespace App\Processors\v1\Files;

use App\Entities\BaseEntity;
use App\Entities\v1\Files\GetFilesResponse;
use App\Entities\v1\Files\DeleteFileRequest;
use App\Entities\v1\Files\PostFileRequest;
use App\Exceptions\Files\FileNotFoundException;
use App\Exceptions\Files\FileSizeException;
use App\Processors\AbstractBaseProcessor;
use App\Repositories\Files\Contracts\FileRepositoryInterface;
use Exception;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Psr\Log\LoggerInterface;

/**
 * Class FileProcessor.
 *
 * @package App\Processors\v1\Files
 */
class FileProcessor extends AbstractBaseProcessor implements FileProcessorInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var BaseEntity
     */
    protected $requestEntity;

    /**
     * @var Collection|null
     */
    protected $responseEntity;

    /**
     * @var Collection|null
     */
    private $data;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * PostFileRequest constructor.
     *
     * @param LoggerInterface $logger
     * @param FileRepositoryInterface $repository
     * @param Filesystem $storage
     */
    public function __construct(
        LoggerInterface $logger,
        FileRepositoryInterface $repository,
        Filesystem $storage
    ) {
        $this->logger = $logger;
        $this->storage = $storage;
        $this->setRepository($repository);
    }

    /**
     * Kicks off the processor and triggers the application to start.
     *
     * @return Response
     */
    public function dispatch(): Response
    {
        switch ($this->request->getMethod()) {
            case Request::METHOD_POST:
                return $this->processPost();
            case Request::METHOD_GET:
                return $this->processGet();
            case Request::METHOD_DELETE:
                return $this->processDelete();
            default:
                return $this->methodNotSupportedResponse();
        }
    }

    /**
     * @return Response
     */
    private function methodNotSupportedResponse(): Response
    {
        return response(null, 404);
    }

    /**
     * @return Response
     */
    private function processPost(): Response
    {
        // TODO add env configurable duplicate file check

        return $this
            ->buildPostRequestEntity()
            ->validateUploadedFile()
            ->addAdditionalFileData()
            ->validateRequestEntity()
            ->postFile()
            ->buildPostFileResponse();
    }

    /**
     * @return FileProcessor
     */
    private function buildPostRequestEntity(): FileProcessor
    {
        $this->requestEntity = (new PostFileRequest())->setFile($this->request->file('file'));

        return $this;
    }

    /**
     * @throws ValidationException
     * @throws FileSizeException
     * @return FileProcessor
     */
    private function validateUploadedFile(): FileProcessor
    {
        $validator = $this->requestEntity->validateFile();
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $validator = $this->requestEntity->validateFileSize();
        if ($validator->fails()) {
            throw new FileSizeException('File size too large');
        }

        return $this;
    }

    /**
     * @throws ValidationException
     * @return FileProcessor
     */
    private function validateRequestEntity(): FileProcessor
    {
        $validator = $this->requestEntity->validate();

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this;
    }

    /**
     * @return FileProcessor
     */
    public function addAdditionalFileData(): FileProcessor
    {
        $this->requestEntity->setName($this->requestEntity->getFile()->getClientOriginalName());
        $this->requestEntity->setType($this->requestEntity->getFile()->getClientOriginalExtension());
        $this->requestEntity->setSize($this->requestEntity->getFile()->getSize());

        return $this;
    }

    /**
     * @throws \PDOException
     * @return FileProcessor
     */
    private function postFile(): FileProcessor
    {
        $model = $this->repository->create([
            'name' => $this->requestEntity->getName(),
            'type' => $this->requestEntity->getType(),
            'size' => $this->requestEntity->getSize()
        ]);

        return $this->persistPostedFile($model->uuid);
    }

    /**
     * @param string $fileName
     * @throws Exception
     * @return FileProcessor
     */
    private function persistPostedFile(string $fileName): FileProcessor
    {
        try {
            $this->requestEntity->getFile()->storeAs(config('file-upload.upload-path'), $fileName);
        } catch (Exception $e) {
            // if file upload failed need to delete created db record
            $this->repository->deleteByUuid($fileName);
            throw new Exception($e->getMessage());
        }

        return $this;
    }

    /**
     * @return Response
     */
    private function buildPostFileResponse(): Response
    {
        return response(null, Response::HTTP_CREATED);
    }

    /**
     * @return Response
     */
    private function processGet(): Response
    {
        return $this
            ->getFilesData()
            ->buildResponseEntity()
            ->buildGetFilesResponse();
    }

    /**
     * @throws \PDOException
     * @return FileProcessor
     */
    private function getFilesData(): FileProcessor
    {
        // TODO implement filters
        // TODO implement pagination
        $this->data = $this->repository->getAll();

        return $this;
    }

    /**
     * @return FileProcessor
     */
    private function buildResponseEntity(): FileProcessor
    {
        $data = collect();

        foreach ($this->data as $file) {
            $data[] = (new GetFilesResponse())->deserialize($file->toArray())->toArray();
        }

        $this->responseEntity = $data;

        return $this;
    }

    /**
     * @return Response
     */
    private function buildGetFilesResponse(): Response
    {
        return response($this->responseEntity->toArray(), Response::HTTP_OK);
    }

    /**
     * @return Response
     */
    private function processDelete(): Response
    {
        return $this
            ->buildDeleteRequestEntity()
            ->validateRequestEntity()
            ->deleteFile()
            ->buildDeleteFileResponse();
    }

    /**
     * @return FileProcessor
     */
    public function buildDeleteRequestEntity(): FileProcessor
    {
        $data = [
            'uuid' => $this->request->uuid
        ];

        $this->requestEntity = (new DeleteFileRequest())->deserialize($data);

        return $this;
    }

    /**
     * @throws FileNotFoundException
     * @throws \PDOException
     * @return FileProcessor
     */
    public function deleteFile(): FileProcessor
    {
        // TODO test race condition on large size files
        if ($this->repository->deleteByUuid($this->requestEntity->getUuid()) === false) {
            throw new FileNotFoundException('File not found');
        }

        $this->storage->delete(config('file-upload.upload-path') . '/' . $this->requestEntity->getUuid());
        $this->logger->info('File deleted: ' . $this->requestEntity->getUuid());

        return $this;
    }

    /**
     * @return Response
     */
    public function buildDeleteFileResponse(): Response
    {
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
