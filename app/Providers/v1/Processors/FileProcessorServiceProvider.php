<?php

namespace App\Providers\v1\Processors;

use App\Processors\v1\Files\FileProcessor;
use App\Processors\v1\Files\FileProcessorInterface;
use App\Repositories\Files\Contracts\FileRepositoryInterface;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

/**
 * Class FileProcessorProvider
 * @package App\Providers\v1\Processors
 */
class FileProcessorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(FileProcessorInterface::class, function (Application $app) {
            $logger = $app->make(LoggerInterface::class);
            $repo = $app->make(FileRepositoryInterface::class);
            $storage = $app->make(Filesystem::class);

            return new FileProcessor($logger, $repo, $storage);
        });
    }
}