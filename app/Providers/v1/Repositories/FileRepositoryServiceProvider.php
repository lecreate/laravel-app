<?php

namespace App\Providers\v1\Repositories;

use App\Models\File;
use App\Repositories\Files\Contracts\FileRepositoryInterface;
use App\Repositories\Files\Eloquent\FileRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class FileRepositoryServiceProvider.
 *
 * @package App\Providers\v1\Repositories
 */
class FileRepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(FileRepositoryInterface::class, function () {
            return new FileRepository(new File());
        });
    }
}
